package com.example.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import com.example.service.LoginService;

@Component
public class AuthenticationService implements AuthenticationProvider {

	@Autowired
	private LoginService loginService;
	
	@Override
	public Authentication authenticate(Authentication auth) throws AuthenticationException {
		String username = auth.getName();
		String passwd = (String) auth.getCredentials();
		
		boolean logueado = loginService.login(username, passwd);
		
		if (logueado) {
			
			return new UsernamePasswordAuthenticationToken(username, passwd, null);
		}
		
		
		return null;
	}

	@Override
	public boolean supports(Class<?> auth) {
		// TODO Auto-generated method stub
		return auth.equals(UsernamePasswordAuthenticationToken.class);
	}

}
